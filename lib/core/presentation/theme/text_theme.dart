import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

TextTheme customTextTheme() {
  final baseTextStyle = GoogleFonts.aBeeZee();
  return TextTheme(
    displayLarge: baseTextStyle.copyWith(fontSize: 57, fontWeight: FontWeight.w400, letterSpacing: -0.2),
    displayMedium: baseTextStyle.copyWith(fontSize: 45, fontWeight: FontWeight.w400),
    displaySmall: baseTextStyle.copyWith(fontSize: 36, fontWeight: FontWeight.w400),
    headlineLarge: baseTextStyle.copyWith(fontSize: 32, fontWeight: FontWeight.w400, letterSpacing: 0.25),
    headlineMedium: baseTextStyle.copyWith(fontSize: 28, fontWeight: FontWeight.w400),
    headlineSmall: baseTextStyle.copyWith(fontSize: 24, fontWeight: FontWeight.w400),
    titleLarge: baseTextStyle.copyWith(fontSize: 22, fontWeight: FontWeight.w500, letterSpacing: 0.15),
    titleMedium: baseTextStyle.copyWith(fontSize: 16, fontWeight: FontWeight.w500, letterSpacing: 0.15),
    titleSmall: baseTextStyle.copyWith(fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 0.1),
    bodyLarge: baseTextStyle.copyWith(fontSize: 16, fontWeight: FontWeight.w400, letterSpacing: 0.5),
    bodyMedium: baseTextStyle.copyWith(fontSize: 14, fontWeight: FontWeight.w400, letterSpacing: 0.25),
    bodySmall: baseTextStyle.copyWith(fontSize: 12, fontWeight: FontWeight.w400, letterSpacing: 0.4),
    labelLarge: baseTextStyle.copyWith(fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 0.1),
    labelMedium: baseTextStyle.copyWith(fontSize: 12, fontWeight: FontWeight.w500, letterSpacing: 0.5),
    labelSmall: baseTextStyle.copyWith(fontSize: 11, fontWeight: FontWeight.w500, letterSpacing: 0.5),
  );
}
