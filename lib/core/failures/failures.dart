class Failure implements Exception {
  final String message;

  const Failure(this.message);
}

class ServerFailure extends Failure {
  ServerFailure(String? message) : super(message ?? 'Sever Error');
}

class CacheFailure extends Failure {
  CacheFailure(super.message);
}

class NoInternetConnectionFailure extends Failure {
  NoInternetConnectionFailure() : super('No Internet Connection');
}

class UnexpectedResponseFailure extends Failure {
  UnexpectedResponseFailure() : super('Unexpected Response From Server');
}

class UnknownFailure extends Failure {
  UnknownFailure() : super('Unknown Failure');
}

class UnauthenticatedFailure extends Failure {
  UnauthenticatedFailure() : super('Please Login First');
}

class BadRequestFailure extends Failure {
  BadRequestFailure(String? message)
      : super(message ?? 'Please Make Sure You Entered Valid Data');
}

class NotFoundFailure extends Failure {
  NotFoundFailure() : super('Not Found');
}

class UnauthorizedFailure extends Failure {
  UnauthorizedFailure(String? message)
      : super(message ?? 'Unauthorized Failure');
}

class ForbiddenFailure extends Failure {
  ForbiddenFailure(String? message) : super(message ?? 'Forbidden Failure');
}
