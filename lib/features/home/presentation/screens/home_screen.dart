// import 'package:algooru_test/core/presentation/utils/responsive_serivce.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_riverpod/flutter_riverpod.dart';
//
// import '../../../../core/presentation/providers/app/app_provider.dart';
//
// class HomeScreen extends ConsumerWidget {
//   const HomeScreen({super.key});
//
//
//   @override
//   Widget build(BuildContext context, WidgetRef ref) {
//
//     bool isDarkMode =
//         ref.watch(applicationProvider).themeMode == ThemeMode.dark;
//     return Scaffold(
//         appBar: AppBar(
//           title: const Text('Home Screen'),
//           actions: [
//             IconButton(
//                 onPressed: () {
//                   isDarkMode
//                       ? ref
//                           .read(applicationProvider.notifier)
//                           .changeTheme(ThemeMode.light)
//                       : ref
//                           .read(applicationProvider.notifier)
//                           .changeTheme(ThemeMode.dark);
//                   ;
//                 },
//                 icon: Icon(isDarkMode ? Icons.light_mode : Icons.dark_mode))
//           ],
//         ),
//         body: SizedBox(
//           width: double.infinity,
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.center,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               SizedBox(height: 20.h),
//             ],
//           ),
//         ));
//   }
// }
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../core/constants/images.dart';
import '../../../../core/failures/failures.dart';
import '../../../../core/presentation/providers/app/app_provider.dart';
import '../widgets/custom_error_widget.dart';
import '../widgets/grid_view_widget.dart';
import '../widgets/list_view_widget.dart';
import 'home_providers.dart';

class HomeScreen extends ConsumerWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isDogView = ref.watch(showDogs);
    final themeMode = ref.watch(applicationProvider).themeMode;

    return Scaffold(
      appBar: AppBar(
        surfaceTintColor: Colors.transparent,
        title: const Text('Algooru Test'),
        leading: IconButton(
          onPressed: () {
            ref.read(applicationProvider.notifier).changeTheme();
          },
          icon: SizedBox(
            height: 30.h,
            width: 30.w,
            child: Icon(themeMode == ThemeMode.light
                ? Icons.dark_mode
                : Icons.light_mode),
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {
              ref.read(showDogs.notifier).state = !ref.read(showDogs);
            },
            icon: SizedBox(
              height: 30.h,
              width: 30.w,
              child: SvgPicture.asset(
                isDogView ? Images.dogs : Images.cats,
                colorFilter: ColorFilter.mode(
                    themeMode == ThemeMode.light ? Colors.black : Colors.white,
                    BlendMode.srcIn),
              ),
            ),
          )
        ],
      ),
      body: !isDogView ? const ListViewWidget() : const GridViewWidget(),
    );
  }
}
