import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'app_state.dart';

final applicationProvider =
    StateNotifierProvider<ApplicationStateNotifier, ApplicationState>(
  (ref) => ApplicationStateNotifier(),
);

class ApplicationStateNotifier extends StateNotifier<ApplicationState> {
  ApplicationStateNotifier() : super(const ApplicationState());

  void changeTheme() {
    if (state.themeMode == ThemeMode.dark) {
      state = state.copyWith(themeMode: ThemeMode.light);
    } else {
      state = state.copyWith(themeMode: ThemeMode.dark);
    }
  }
}
