import 'package:flutter/material.dart';

import 'color_scheme.dart';

abstract class AppColorScheme {
  static const Color primaryAppColor = Color(0xFF47b978);
  static CustomColorScheme lightColorScheme = CustomColorScheme(
    primary: primaryAppColor,
    primaryVariant: Colors.teal[700]!, // A darker shade of primary
    secondary: Colors.orangeAccent, // Complementary secondary color
    secondaryVariant: Colors.orangeAccent[700]!, // Darker shade of secondary
    surface: Colors.white,
    background: Colors.grey[100]!, // A lighter background
    error: Colors.redAccent, // Softer error color
    onPrimary: Colors.white,
    onSecondary: Colors.black,
    onSurface: Colors.black,
    onBackground: Colors.black,
    onError: Colors.black,
    brightness: Brightness.light,
  );

  static CustomColorScheme darkColorScheme = CustomColorScheme(
    primary: primaryAppColor,
    primaryVariant: Colors.teal[800]!, // Even darker shade for dark theme
    secondary: Colors.deepOrange[700]!, // Secondary color for dark theme
    secondaryVariant: Colors.deepOrange[900]!, // Darker shade of secondary
    surface: Colors.grey[800]!, // Dark surface color
    background: Colors.grey[850]!, // Dark background color
    error: Colors.red[800]!, // Darker error color
    onPrimary: Colors.white,
    onSecondary: Colors.white,
    onSurface: Colors.white,
    onBackground: Colors.white,
    onError: Colors.white,
    brightness: Brightness.dark,
  );
}
