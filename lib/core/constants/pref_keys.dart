abstract class PrefKeys {
  static const String isLoggedIn = "is_logged_in";
  static const String accessToken = "access_token";
  static const String refreshToken = "refresh_token";
}
