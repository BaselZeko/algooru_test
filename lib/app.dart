import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'core/presentation/providers/app/app_provider.dart';
import 'core/presentation/theme/app_color_schems.dart';
import 'features/home/presentation/screens/home_screen.dart';

class App extends ConsumerWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final themeMode = ref.watch(applicationProvider).themeMode;
    return ScreenUtilInit(
      designSize: const Size(480, 845),
      minTextAdapt: true,
      splitScreenMode: true,
      child: MaterialApp(
        title: 'AlGooru Test',
        debugShowCheckedModeBanner: false,
        theme: AppColorScheme.lightColorScheme.themeData,
        darkTheme: AppColorScheme.darkColorScheme.themeData,
        themeMode: themeMode,
        home: const HomeScreen(),
      ),
    );
  }
}
