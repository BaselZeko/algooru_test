import 'dart:convert';
import 'dart:io';

import 'package:algooru_test/core/data/model/base_response.dart';
import 'package:algooru_test/features/home/data/datasource/home_remote_datasource.dart';
import 'package:algooru_test/features/home/data/datasource/home_remote_datasource_impl.dart';
import 'package:algooru_test/features/home/data/model/dog_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockHomeRemoteDataSource extends Mock implements HomeRemoteDataSource {}

class MockDio extends Mock implements Dio {}

void main() {
  late MockHomeRemoteDataSource mockHomeRemoteDataSource;
  late MockDio mockDio;
  late HomeRemoteDataSourceImpl homeRemoteDataSourceImpl;

  setUp(() {
    mockDio = MockDio();
    mockHomeRemoteDataSource = MockHomeRemoteDataSource();
    homeRemoteDataSourceImpl = HomeRemoteDataSourceImpl(dio: mockDio);
  });

  group('getDogList', () {
    test(
        'should return a list of DogModel when the call to data source is successful',
        () async {
      var jsonString = File('test/resources/dog_list_mock_data.json').readAsStringSync();

      var jsonData = jsonDecode(jsonString) as List;

      var mockDogList =
          jsonData.map((json) => DogModel.fromJson(json)).toList();

      var mockDogListData = BaseResponse(data: mockDogList);

      when(mockHomeRemoteDataSource.getDogList()).thenAnswer((_) async {
        try {
          return Future.value(mockDogListData);
        } catch (e) {
          print('Error in mock setup: $e');
          return Future.value(null); // Or handle the error appropriately
        }
      });

      final result = await homeRemoteDataSourceImpl.getDogList();

      expect(result, isA<List<DogModel>>());
    });
  });

  group('getCatList', () {
    test(
        'should return a list of CatModel when the call to data source is successful',
            () async {
          var jsonString = File('test/resources/cat_list_mock_data.json').readAsStringSync();

          var jsonData = jsonDecode(jsonString) as List;

          var mockDogList =
          jsonData.map((json) => DogModel.fromJson(json)).toList();

          var mockDogListData = BaseResponse(data: mockDogList);

          when(mockHomeRemoteDataSource.getDogList()).thenAnswer((_) async {
            try {
              return Future.value(mockDogListData);
            } catch (e) {
              print('Error in mock setup: $e');
              return Future.value(null); // Or handle the error appropriately
            }
          });

          final result = await homeRemoteDataSourceImpl.getDogList();

          expect(result, isA<List<DogModel>>());
        });
  });
}
