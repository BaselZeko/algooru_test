import 'package:algooru_test/features/home/data/model/cat_model.dart';
import 'package:algooru_test/features/home/data/model/dog_model.dart';
import 'package:injectable/injectable.dart';

import '../../domain/repository/home_repository.dart';
import '../datasource/home_remote_datasource.dart';

@Singleton(as: HomeRepository)
class HomeRepositoryImpl implements HomeRepository {
  final HomeRemoteDataSource homeRemoteDataSource;

  HomeRepositoryImpl(this.homeRemoteDataSource);

  @override
  Future<List<DogModel>> getDogList() async {
    final res = await homeRemoteDataSource.getDogList();
    return res.data!;
  }

  @override
  Future<List<CatModel>> getCatList() async {
    final res = await homeRemoteDataSource.getCatList();
    return res.data!;
  }
}
