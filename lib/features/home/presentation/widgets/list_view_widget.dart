import 'package:algooru_test/features/home/presentation/widgets/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/failures/failures.dart';
import '../../data/model/dog_model.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../screens/home_providers.dart';
import 'custom_error_widget.dart';

class ListViewWidget extends ConsumerWidget {
  const ListViewWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final dogsValue = ref.watch(dogApiProvider);
    return dogsValue.when(
        data: (dogs) => RefreshIndicator(
              onRefresh: () async => ref.refresh(dogApiProvider),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10.h),
                child: Column(
                  children: [
                    Expanded(
                      child: ListView.separated(
                        physics: const BouncingScrollPhysics(),
                        itemCount: dogs.length,
                        padding:
                            EdgeInsetsDirectional.symmetric(horizontal: 20.w),
                        itemBuilder: (context, index) {
                          if (index == dogs.length - 1) {
                            return ElevatedButton(
                              onPressed: () => {},
                              child: const Text('End of List Button'),
                            );
                          } else {
                            return DogListItem(dog: dogs[index]);
                          }
                        },
                        separatorBuilder: (BuildContext context, int index) {
                          return SizedBox(
                            height: 20.h,
                          );
                        },
                      ),
                    ),
                    if (dogs.isEmpty)
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: ElevatedButton(
                          onPressed: () => {},
                          child: const Text('List is empty'),
                        ),
                      ),
                  ],
                ),
              ),
            ),
        loading: () =>
            const Center(child: CircularProgressIndicator.adaptive()),
        error: (error, stackTrace) => CustomErrorWidget(
            errorMessage: error as Failure,
            onRetry: () => ref.refresh(dogApiProvider)),
        skipLoadingOnRefresh: false,
        skipLoadingOnReload: false);
  }
}

class DogListItem extends StatelessWidget {
  final DogModel dog;

  const DogListItem({super.key, required this.dog});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.r),
        border: Border.all(
          color: Colors.grey.shade200,
          width: 1.w,
        ),
      ),
      child: SizedBox(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 100.w,
              height: 100.h,
              clipBehavior: Clip.antiAlias,
              // Enables clipping with anti-aliasing
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.circular(10.r), // Same radius as parent
              ),
              child: CachedNetworkImage(
                imageUrl: dog.url!,
                fit: BoxFit.cover,
                placeholder: (context, url) => const LoadingWidget(),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(dog.breeds!.first.name!,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context)
                            .textTheme
                            .headlineSmall!
                            .copyWith(fontSize: 24.sp)),
                    SizedBox(
                      height: 5.h,
                    ),

                    //No Description in response

                    Text(
                      dog.breeds!.first.temperament ?? "",
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context)
                          .textTheme
                          .labelSmall!
                          .copyWith(fontSize: 16.sp),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
