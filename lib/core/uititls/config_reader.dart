import 'dart:convert';

import 'package:flutter/services.dart';

class ConfigReader {
  static late Map<String, dynamic> _config;

  static Future<void> initialize() async {
    final configString = await rootBundle.loadString('config/app_config.json');
    _config = json.decode(configString) as Map<String, dynamic>;
  }

  static String getBaseUrl() => _config["baseUrl"];

  static String getDogApiKey() => _config['dogApiKey'];

  static String getCatApiKey() => _config['catApiKey'];

}
