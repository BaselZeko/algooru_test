import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:injectable/injectable.dart';
import 'app.dart';
import 'core/uititls/config_reader.dart';
import 'di/modules/injectable_module.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await ConfigReader.initialize();
  await inject(environment: Environment.dev);
  runApp(const ProviderScope(child: App()));
}
