import 'package:algooru_test/core/usecase/usecase.dart';
import 'package:algooru_test/features/home/data/model/dog_model.dart';
import 'package:algooru_test/features/home/domain/repository/home_repository.dart';
import 'package:algooru_test/features/home/domain/usecase/get_cat_list_use_case.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:algooru_test/features/home/data/model/cat_model.dart';

class MockHomeRepository extends Mock implements HomeRepository {}

void main() {
  late GetCatListUseCase useCase;
  late MockHomeRepository mockHomeRepository;
  final List<DogModel> tDogList = []; // Replace with actual test data
  final List<CatModel> tCatList = []; // Replace with actual test data

  setUp(() {
    mockHomeRepository = MockHomeRepository();
    useCase = GetCatListUseCase(repository: mockHomeRepository);
  });

  test('should get list of cats from the repository', () async {
    // Arrange
    when(mockHomeRepository.getDogList()).thenAnswer((_) async => tDogList);

    // Act
    final result = await useCase(NoParams());

    // Assert
    expect(result, equals(tCatList));
    verify(mockHomeRepository.getDogList()).called(1);
  });

  test('should get list of cats from the repository', () async {
    // Arrange
    when(mockHomeRepository.getCatList()).thenAnswer((_) async => tCatList);

    // Act
    final result = await useCase(NoParams());

    // Assert
    expect(result, equals(tCatList));
    verify(mockHomeRepository.getCatList()).called(1);
  });
}
