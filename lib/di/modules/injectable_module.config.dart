// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i3;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i8;

import '../../core/data/datasource/base_remote_data_source_impl.dart' as _i9;
import '../../features/home/data/datasource/home_remote_datasource.dart' as _i4;
import '../../features/home/data/datasource/home_remote_datasource_impl.dart'
    as _i5;
import '../../features/home/data/repository/home_repository_impl.dart' as _i7;
import '../../features/home/domain/repository/home_repository.dart' as _i6;
import '../../features/home/domain/usecase/get_cat_list_use_case.dart' as _i10;
import '../../features/home/domain/usecase/get_dog_list_use_case.dart' as _i11;
import 'injectable_module.dart' as _i12;

// initializes the registration of main-scope dependencies inside of GetIt
_i1.GetIt $inject(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  final appModule = _$AppModule();
  gh.singleton<_i3.BaseOptions>(appModule.dioOption());
  gh.singleton<_i3.Dio>(appModule.dio(gh<_i3.BaseOptions>()));
  gh.singleton<_i4.HomeRemoteDataSource>(
      _i5.HomeRemoteDataSourceImpl(dio: gh<_i3.Dio>()));
  gh.singleton<_i6.HomeRepository>(
      _i7.HomeRepositoryImpl(gh<_i4.HomeRemoteDataSource>()));
  gh.singletonAsync<_i8.SharedPreferences>(() => appModule.getPrefs());
  gh.lazySingleton<_i9.BaseRemoteDataSourceImpl>(
      () => _i9.BaseRemoteDataSourceImpl(dio: gh<_i3.Dio>()));
  gh.factory<_i10.GetCatListUseCase>(
      () => _i10.GetCatListUseCase(repository: gh<_i6.HomeRepository>()));
  gh.factory<_i11.GetDogListUseCase>(
      () => _i11.GetDogListUseCase(repository: gh<_i6.HomeRepository>()));
  return getIt;
}

class _$AppModule extends _i12.AppModule {}
