import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/uititls/dio/app_interceptor.dart';
import 'injectable_module.config.dart';

final GetIt getIt = GetIt.I;

@InjectableInit(
  initializerName: r'$inject',
  preferRelativeImports: true,
  asExtension: false,
)
Future<GetIt> inject({String environment = Environment.dev}) async =>
    $inject(getIt);

@module
abstract class AppModule {
  @singleton
  BaseOptions dioOption() => BaseOptions(
        connectTimeout: const Duration(seconds: 5),
        receiveTimeout: const Duration(seconds: 5),
        contentType: 'application/json;charset=utf-8',
        responseType: ResponseType.plain,
        headers: <String, String>{
          'Accept': 'application/json',
          'Connection': 'keep-alive',
        },
      );

  @singleton
  Future<SharedPreferences> getPrefs() => SharedPreferences.getInstance();

  @singleton
  Dio dio(
    BaseOptions option,
  ) {
    final Dio dio = Dio(option);
    return dio
      ..interceptors.addAll(<Interceptor>[
        AppInterceptor(),
      ]);
  }
}
