import 'dart:developer';
import 'package:algooru_test/core/data/datasource/base_remote_datasource.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';

import '../../failures/failures.dart';
import '../../uititls/config_reader.dart';
import '../model/base_response.dart';

@lazySingleton
class BaseRemoteDataSourceImpl implements BaseRemoteDataSource {
  Dio dio;

  BaseRemoteDataSourceImpl({required this.dio});

  @override
  Future<BaseResponse<T>> post<T>({
    required String url,
    Map<String, dynamic>? body,
    Map<String, dynamic>? params,
    Map<String, dynamic>? headers,
    required T Function(dynamic) decoder,
    bool requiredToken = true,
  }) async {
    try {
      final res = await dio.post(
        url,
        data: body,
        queryParameters: params,
        options: Options(
          headers: headers,
        ),
      );
      return BaseResponse<T>(
        data: decoder(res.data),
      );
    } catch (e) {
      throw UnexpectedResponseFailure();
    }
  }

  @override
  Future<BaseResponse<T>> get<T>({
    required String url,
    Map<String, dynamic>? params,
    required T Function(dynamic) decoder,
    bool requiredToken = true,
    bool withApiKey = true,
    String apiKey = '',
  }) async {
    try {
      if (withApiKey) {
        dio.options.headers.addAll({"x-api-key": apiKey});
      }
      final res = await dio.get(
        url,
        queryParameters: params,
      );

      return BaseResponse<T>(
        data: decoder(res.data),
      );
    } catch (e) {
      throw UnexpectedResponseFailure();
    }
  }

  @override
  Future<BaseResponse<T>> delete<T>({
    required String url,
    Map<String, dynamic>? params,
    required T Function(dynamic) decoder,
    bool requiredToken = true,
  }) async {
    try {
      final res = await dio.delete(
        url,
        queryParameters: params,
      );
      return BaseResponse<T>(
        data: decoder(res.data),
      );
    } catch (e) {
      throw UnexpectedResponseFailure();
    }
  }

  @override
  Future<BaseResponse<T>> put<T>({
    required String url,
    Map<String, dynamic>? body,
    Map<String, dynamic>? params,
    required T Function(dynamic) decoder,
    bool requiredToken = true,
  }) async {
    try {
      final res = await dio.put(
        url,
        data: body,
        queryParameters: params,
      );
      return BaseResponse<T>(
        data: decoder(res.data),
      );
    } catch (e) {
      throw UnexpectedResponseFailure();
    }
  }
}
