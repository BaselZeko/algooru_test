import 'package:algooru_test/features/home/data/model/dog_model.dart';

import '../../../../core/data/model/base_response.dart';
import '../model/cat_model.dart';

abstract class HomeRemoteDataSource {
  Future<BaseResponse<List<DogModel>>> getDogList();

  Future<BaseResponse<List<CatModel>>> getCatList();
}
