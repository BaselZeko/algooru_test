import 'dart:convert';

import 'package:algooru_test/core/data/datasource/base_remote_data_source_impl.dart';
import 'package:algooru_test/core/data/model/base_response.dart';
import 'package:algooru_test/features/home/data/model/cat_model.dart';
import 'package:algooru_test/features/home/data/model/dog_model.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/constants/endpoints.dart';
import '../../../../core/data/datasource/base_remote_datasource.dart';
import '../../../../core/uititls/config_reader.dart';
import 'home_remote_datasource.dart';

@Singleton(as: HomeRemoteDataSource)
class HomeRemoteDataSourceImpl extends BaseRemoteDataSourceImpl
    implements HomeRemoteDataSource {
  HomeRemoteDataSourceImpl({required super.dio});

  @override
  Future<BaseResponse<List<DogModel>>> getDogList() async {
    return get(
      url: Endpoints.getDogApi,
      apiKey: ConfigReader.getDogApiKey(),
      withApiKey: true,
      decoder: (json) {
        var dogsJson = jsonDecode(json['data']) as List;
        return dogsJson.map((item) => DogModel.fromJson(item)).toList();
      },
      requiredToken: false,
    );
  }

  @override
  Future<BaseResponse<List<CatModel>>> getCatList() {
    return get(
      url: Endpoints.getCatApi,
      apiKey: ConfigReader.getCatApiKey(),
      withApiKey: true,
      decoder: (json) {
        var catJson = jsonDecode(json['data']) as List;
        return catJson.map((item) => CatModel.fromJson(item)).toList();
      },
      requiredToken: false,
    );
  }
}
