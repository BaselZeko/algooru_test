import 'package:algooru_test/features/home/data/model/cat_model.dart';
import 'package:algooru_test/features/home/data/model/dog_model.dart';

abstract class HomeRepository {
  Future<List<DogModel>> getDogList();
  Future<List<CatModel>> getCatList();
}
