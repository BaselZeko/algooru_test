import 'package:algooru_test/features/home/data/model/cat_model.dart';
import 'package:algooru_test/features/home/data/model/dog_model.dart';
import 'package:algooru_test/features/home/domain/repository/home_repository.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/usecase/usecase.dart';

@injectable
class GetCatListUseCase extends UseCase<List<CatModel>, NoParams> {
  final HomeRepository repository;

  GetCatListUseCase({required this.repository});

  @override
  Future<List<CatModel>> call(params) async {
    var result = await repository.getCatList();

    return result;
  }
}
