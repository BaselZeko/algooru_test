import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io' show Platform;

import 'package:flutter_screenutil/flutter_screenutil.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Platform.isIOS
        ? const CupertinoActivityIndicator()
        : SizedBox(
            height: 50.h,
            width: 50.w,
            child: const Center(
                child: CircularProgressIndicator(
              strokeWidth: 1,
            )),
          );
  }
}
