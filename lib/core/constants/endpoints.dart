import '../uititls/config_reader.dart';

abstract class Endpoints {
  String get _baseUrl => ConfigReader.getBaseUrl();

  static String get getDogApi =>
      "https://api.thedogapi.com/v1/images/search?size=med&mime_types=jpg&format=json&has_breeds=true&order=RANDOM&page=0&limit=20";

  static String get getCatApi =>
      "https://api.thecatapi.com/v1/images/search?size=med&mime_types=jpg&format=json&has_breeds=true&order=RANDOM&page=0&limit=20";
}
