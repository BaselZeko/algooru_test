import 'package:algooru_test/features/home/data/model/dog_model.dart';
import 'package:algooru_test/features/home/domain/repository/home_repository.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/usecase/usecase.dart';

@injectable
class GetDogListUseCase extends UseCase<List<DogModel>, NoParams> {
  final HomeRepository repository;

  GetDogListUseCase({required this.repository});

  @override
  Future<List<DogModel>> call(params) async {
    var result = await repository.getDogList();

    return result;
  }
}
