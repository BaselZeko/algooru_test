import 'package:algooru_test/features/home/presentation/screens/home_providers.dart';
import 'package:algooru_test/features/home/presentation/widgets/loading_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../../../core/failures/failures.dart';
import 'custom_error_widget.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class GridViewWidget extends ConsumerWidget {
  const GridViewWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final catValue = ref.watch(catApiProvider);
    return catValue.when(
        data: (data) {
          return RefreshIndicator(
            onRefresh: () async => ref.refresh(catApiProvider),
            child: GridView.builder(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
              ),
              itemCount: data.length,
              itemBuilder: (context, index) {
                final item = data[index];
                double aspectRatio =
                    item.width!.toDouble() / item.height!.toDouble();

                return LayoutBuilder(
                  builder: (context, constraints) {
                    double width = (constraints.maxWidth / 2) -
                        10; // 10 is for padding, adjust as needed
                    double height = width / aspectRatio;
                    return CachedNetworkImage(
                      imageUrl: item.url!,
                      height: height,
                      width: width,
                      fit: BoxFit.cover,
                      placeholder: (context, url) =>
                          const LoadingWidget(),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error),
                    );
                  },
                );
              },
            ),
          );
        },
        error: (error, stackTrace) {
          return CustomErrorWidget(
              errorMessage: error as Failure,
              onRetry: () => ref.refresh(catApiProvider));
        },
        loading: () =>
            const Center(child: CircularProgressIndicator.adaptive()),
        skipLoadingOnRefresh: false,
        skipLoadingOnReload: false);
  }
}
//  @override
//   Widget build(BuildContext context, WidgetRef ref) {
//     final catValue = ref.watch(catApiProvider);
//     return catValue.when(
//       data: (data) {
//         return SingleChildScrollView(
//           child: StaggeredGrid.count(
//             crossAxisCount: 2,
//             mainAxisSpacing: 0,
//             crossAxisSpacing: 0,
//             children: data
//                 .map(
//                   (e) => CachedNetworkImage(
//                     imageUrl: e.url!,
//                     height: e.height?.toDouble(),
//                     width: e.width?.toDouble(),
//                     placeholder: (context, url) =>
//                         const CircularProgressIndicator.adaptive(),
//                     errorWidget: (context, url, error) => const Icon(Icons.error),
//                   ),
//                 )
//                 .toList(),
//           ),
//         );
//       },
//       error: (error, stackTrace) {
//         return CustomErrorWidget(
//           errorMessage: error as Failure,
//         );
//       },
//       loading: () => const Center(child: CircularProgressIndicator.adaptive()),
//     );
//   }
// }
