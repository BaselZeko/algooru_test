import 'package:algooru_test/core/presentation/theme/text_theme.dart';
import 'package:flutter/material.dart';

class CustomColorScheme {
  final Color primary;
  final Color primaryVariant;
  final Color secondary;
  final Color secondaryVariant;
  final Color surface;
  final Color background;
  final Color error;
  final Color onPrimary;
  final Color onSecondary;
  final Color onSurface;
  final Color onBackground;
  final Color onError;
  final Brightness brightness;

  CustomColorScheme({
    required this.primary,
    required this.primaryVariant,
    required this.secondary,
    required this.secondaryVariant,
    required this.surface,
    required this.background,
    required this.error,
    required this.onPrimary,
    required this.onSecondary,
    required this.onSurface,
    required this.onBackground,
    required this.onError,
    this.brightness = Brightness.light,
  });

  ThemeData get themeData {
    return ThemeData.from(
        colorScheme: toColorScheme(), textTheme: customTextTheme());
  }

  ColorScheme toColorScheme() {
    return ColorScheme(
      primary: primary,
      primaryContainer: primaryVariant,
      secondary: secondary,
      secondaryContainer: secondaryVariant,
      surface: surface,
      background: background,
      error: error,
      onPrimary: onPrimary,
      onSecondary: onSecondary,
      onSurface: onSurface,
      onBackground: onBackground,
      onError: onError,
      brightness: brightness,
    );
  }
}
