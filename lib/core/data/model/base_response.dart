class BaseResponse<T> {
  final T? data;

  BaseResponse({this.data});

  factory BaseResponse.fromJson(dynamic json, T Function(dynamic)? decoder) {
    if (decoder == null) {
      return BaseResponse(data: json);
    }
    return BaseResponse(
      data: decoder(json[kDataKey]),
    );
  }
}

const kDataKey = 'data';
