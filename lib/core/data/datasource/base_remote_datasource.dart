
import 'package:flutter/material.dart';

import '../model/base_response.dart';

enum Method { GET, POST, PUT, DELETE }

abstract class BaseRemoteDataSource {
  @protected
  Future<BaseResponse<T>> post<T>(
      {required String url,
        Map<String, dynamic>? body,
        Map<String, dynamic>? params,
        Map<String, dynamic>? headers,
        required T Function(dynamic) decoder,
        bool requiredToken = true});

  @protected
  Future<BaseResponse<T>> get<T>(
      {required String url,
        Map<String, dynamic>? params,
        required T Function(dynamic) decoder,
        bool requiredToken = true});

  @protected
  Future<BaseResponse<T>> delete<T>(
      {required String url,
        Map<String, dynamic>? params,
        required T Function(dynamic) decoder,
        bool requiredToken = true});

  @protected
  Future<BaseResponse<T>> put<T>(
      {required String url,
        Map<String, dynamic>? body,
        Map<String, dynamic>? params,
        required T Function(dynamic p1) decoder,
        bool requiredToken = true});
}

