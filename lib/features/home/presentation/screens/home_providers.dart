import 'package:algooru_test/features/home/data/model/cat_model.dart';
import 'package:algooru_test/features/home/data/model/dog_model.dart';
import 'package:algooru_test/features/home/domain/usecase/get_cat_list_use_case.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../../../core/usecase/usecase.dart';
import '../../../../di/modules/injectable_module.dart';
import '../../domain/usecase/get_dog_list_use_case.dart';

final dogApiProvider = FutureProvider<List<DogModel>>((ref) async {
  final response = await getIt<GetDogListUseCase>()(NoParams());
  return response;
});


final catApiProvider = FutureProvider<List<CatModel>>((ref) async {
  final response = await getIt<GetCatListUseCase>()(NoParams());
  return response;
});

final showDogs = StateProvider<bool>((ref) => false);
