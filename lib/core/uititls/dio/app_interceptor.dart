import 'package:dio/dio.dart';

import '../../data/model/base_response.dart';
import '../../failures/failures.dart';

class AppInterceptor extends Interceptor {
  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    if (response.data is! Map || !response.data.containsKey(kDataKey)) {
      response.data = {kDataKey: response.data};
    }
    super.onResponse(response, handler);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    if (err.type == DioExceptionType.unknown) {
      if (err.response?.statusCode == 401) {
        err = err.copyWith(error: UnauthenticatedFailure());
      } else if (err.response?.statusCode == 400) {
        err = err.copyWith(error: BadRequestFailure(err.message));
      } else if (err.response?.statusCode == 404) {
        err = err.copyWith(error: NotFoundFailure());
      } else if (err.response?.statusCode == 500) {
        err = err.copyWith(
            error: ServerFailure(err.message ?? 'Unknown Server Error'));
      }
    } else if (err.type == DioExceptionType.connectionError ||
        err.type == DioExceptionType.sendTimeout ||
        err.type == DioExceptionType.receiveTimeout ||
        err.type == DioExceptionType.cancel) {
      err = err.copyWith(error: NoInternetConnectionFailure());
    }
    handler.next(err);
  }
}
